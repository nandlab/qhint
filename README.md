# Install Qt MQTT
```
sudo apt install qtbase5-private-dev

git clone https://code.qt.io/qt/qtmqtt.git -b 5.15.2
cd qtmqtt
mkdir build
cd build
qmake ..
make
sudo make install
```

# Install QHint
```
sudo apt install cmake

git clone https://gitlab.com/nandlab/qhint.git -b devel
cd qhint
mkdir build
cd build
cmake ..
make
```
