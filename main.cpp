#include "widget.h"

#include <QApplication>
#include <QSettings>

#include <iostream>
#include <list>

int main(int argc, char *argv[])
{
    using std::cout, std::cerr, std::endl, std::list;

    QApplication a(argc, argv);
    QSettings settings("locked", "qhint");

    settings.beginGroup("broker");
    QString brokerHostname = settings.value("hostname", "localhost").toString();
    quint16 brokerPort = settings.value("port", 1883).toUInt();
    settings.endGroup();

    QString clientId = settings.value("clientId", "qhint").toString();
    qreal fontSize = settings.value("fontSize", 50).toReal();
    QString logFile = settings.value("logFile", "").toString();

    list<QString> backgrounds;
    int size = settings.beginReadArray("backgrounds");

    /* if (size <= 0) {
        cerr << "At least one background image should be specified!" << endl;
        exit(1);
    } */

    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        backgrounds.emplace_back(settings.value("bg").toString());
    }
    settings.endArray();

    // line height in percent
    qreal lineHeight = settings.value("lineHeight", 100).toReal();

    cout << "Broker hostname: " << brokerHostname.toStdString() << endl;
    cout << "Broker port: " << brokerPort << endl;
    cout << "Client ID: " << clientId.toStdString() << endl;

    if (size <= 0) {
        cout << "No background files" << endl;
    }
    else {
        cout << "Background files:" << endl;
        int i = 0;
        list<QString>::const_iterator iter = backgrounds.cbegin();
        for (; i < size; ++i, ++iter) {
            cout << i << ": " << iter->toStdString() << endl;
        }
    }

    Widget w(brokerHostname, brokerPort, clientId, backgrounds, fontSize, lineHeight, logFile);
    w.showFullScreen();
    w.startMqttConnection();
    return a.exec();
}
