#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMqttClient>
#include <QTimer>
#include <QMqttMessage>
#include <list>
#include <QFile>

class Widget : public QWidget
{
    Q_OBJECT

    QMqttClient mqttClient;
    QTimer reconnectTimer;
    std::vector<QPixmap> backgroundPixmaps;
    int selectedBackground;
    QString hint;
    qreal fontSize;
    qreal lineHeight;
    QFile log;
    QTextStream logStream;

public:
    Widget(const QString &brokerHostname = "localhost",
           quint16 brokerPort = 1883,
           const QString &clientId = "qhint",
           const std::list<QString> backgrounds = {},
           qreal fontSize = 50,
           qreal lineHeight = 150,
           QString logFile = "",
           QWidget *parent = nullptr);

    void paintEvent(QPaintEvent *paintEvent) override;

    void keyPressEvent(QKeyEvent *event) override;

    void startMqttConnection();

public slots:
    void stateChanged(QMqttClient::ClientState state);

    void connected();

    void disconnected();

    void errorChanged(QMqttClient::ClientError error);

    void messageSent(qint32 id);

    void reconnect();

    void disconnect();

    void messageReceived(QMqttMessage message);
};
#endif // WIDGET_H
