#include "widget.h"
#include <iostream>
#include <QTextDocument>
#include <QTextCursor>
#include <QPainter>
#include <QPixmap>
#include <QApplication>
#include <QMqttTopicFilter>
#include <QKeyEvent>
#include <iomanip>

#define MQTT_TOPIC_STATUS "/status"
#define MQTT_TOPIC_HINT "/set/hint"
#define MQTT_TOPIC_BACKGROUND "/set/background"

#define MQTT_STATUS_WILL "FAILED"
#define MQTT_STATUS_ONLINE "ONLINE"
#define MQTT_STATUS_OFFLINE "OFFLINE"

static std::vector<QPixmap> buildPixmaps(const std::list<QString> &pics) {
    std::vector<QPixmap> pixmaps;
    for (const QString &pic : pics) {
        pixmaps.emplace_back(pic);
    }
    return pixmaps;
}

Widget::Widget(
        const QString &brokerHostname,
        quint16 brokerPort,
        const QString &clientId,
        const std::list<QString> backgrounds,
        qreal fontSize,
        qreal lineHeight,
        QString logFile,
        QWidget *parent
)
    : QWidget(parent)
    , mqttClient()
    , reconnectTimer()
    , backgroundPixmaps(buildPixmaps(backgrounds))
    , hint()
    , fontSize(fontSize)
    , lineHeight(lineHeight)
    , log()
    , logStream(&log)
{
    mqttClient.setHostname(brokerHostname);
    mqttClient.setPort(brokerPort);
    mqttClient.setClientId(clientId);
    mqttClient.setWillTopic(clientId + MQTT_TOPIC_STATUS);
    mqttClient.setWillMessage("FAILED");
    mqttClient.setWillRetain(true);

    if (logFile.size()) {
        log.setFileName(logFile);
        log.open(QIODevice::WriteOnly | QIODevice::Append);
    }

    reconnectTimer.setSingleShot(true);
    reconnectTimer.setInterval(1000);

    connect(QApplication::instance(), &QApplication::aboutToQuit, this, &Widget::disconnect);

    connect(&reconnectTimer, &QTimer::timeout, this, &Widget::reconnect);

    connect(&mqttClient, &QMqttClient::stateChanged, this, &Widget::stateChanged);
    connect(&mqttClient, &QMqttClient::connected, this, &Widget::connected);
    connect(&mqttClient, &QMqttClient::disconnected, this, &Widget::disconnected);
    connect(&mqttClient, &QMqttClient::errorChanged, this, &Widget::errorChanged);
    connect(&mqttClient, &QMqttClient::messageSent, this, &Widget::messageSent);

    if (backgroundPixmaps.size()) {
        selectedBackground = 0;
    }
    else {
        selectedBackground = -1;
    }
}

void Widget::startMqttConnection() {
    std::cout << "Starting MQTT connection ..." << std::endl;
    mqttClient.connectToHost();
}

void Widget::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_F) {
        isFullScreen() ? showNormal() : showFullScreen();
    }
}

void Widget::paintEvent(QPaintEvent *paintEvent)
{
    (void) paintEvent;

    QPainter painter(this);
    painter.setBrush(QColorConstants::Black);

    if (hint.size()) {
        painter.drawRect(rect());
        QTextDocument document;
        QTextCursor cursor(&document);
        document.setTextWidth(width());
        {
            QTextBlockFormat blockFormat;
            blockFormat.setAlignment(Qt::AlignCenter);
            blockFormat.setLineHeight(lineHeight, QTextBlockFormat::ProportionalHeight);
            cursor.setBlockFormat(blockFormat);
        }
        {
            QTextCharFormat charFormat;
            // charFormat.setTextOutline(QPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)));
            charFormat.setForeground(Qt::white);
            charFormat.setFontPointSize(fontSize);
            cursor.setCharFormat(charFormat);
        }
        cursor.insertText(hint);
        painter.translate(0, (height() - document.size().height()) / 2.0);
        document.drawContents(&painter);
    }
    else {
        if (selectedBackground >= 0) {
            painter.drawPixmap(rect(), backgroundPixmaps[selectedBackground]);
        }
        else {
            painter.drawRect(rect());
        }
    }
}

void Widget::stateChanged(QMqttClient::ClientState state) {
    std::cout << "State: " << state << std::endl;
}

void Widget::connected() {
    std::cout << "Connected" << std::endl;

    QString clientId = mqttClient.clientId();
    QString topicStatus = clientId + MQTT_TOPIC_STATUS;

    std::cout << "Subscribing to setter topics" << std::endl;
    QMqttSubscription *sub = mqttClient.subscribe(clientId + "/set/#");
    std::cout << "Sending status: " << MQTT_STATUS_ONLINE << std::endl;
    mqttClient.publish(topicStatus, MQTT_STATUS_ONLINE, 0, true);
    connect(sub, &QMqttSubscription::messageReceived, this, &Widget::messageReceived);
}

void Widget::disconnected() {
    std::cout << "Disconnected" << std::endl;
    reconnectTimer.start();
}

void Widget::errorChanged(QMqttClient::ClientError error) {
    std::cout << "Error: " << error << std::endl;
}

void Widget::messageSent(qint32 id) {
    std::cout << "Message with id " << id << " was sent" << std::endl;
}

void Widget::reconnect() {
    std::cout << "Reconnecting ..." << std::endl;
    mqttClient.connectToHost();
}

void Widget::disconnect() {
    std::cout << "Sending status: " << MQTT_STATUS_OFFLINE << std::endl;
    mqttClient.publish(mqttClient.clientId() + MQTT_TOPIC_STATUS, MQTT_STATUS_OFFLINE, 0, true);
}

void Widget::messageReceived(QMqttMessage message) {
    QString clientId = mqttClient.clientId();
    QString topic = message.topic().name();
    QString payload = message.payload();
    bool refresh = true;
    if (topic == clientId + MQTT_TOPIC_HINT) {
        hint = payload;
        if (hint.size()) {
            std::cout << "Showing hint: " << std::quoted(hint.toStdString()) << std::endl;
            if (logStream.status() == QTextStream::Status::Ok) {
                logStream << QDateTime::currentDateTimeUtc().toString(Qt::ISODateWithMs) << "," << hint.size() << Qt::endl;
            }
        }
        else {
            std::cout << "Clear hint" << std::endl;
        }
    }
    else if (topic == clientId + MQTT_TOPIC_BACKGROUND) {
        bool ok;
        selectedBackground = payload.toInt(&ok);
        if (!ok) {
            std::cout << "Warning: Cannot parse background index, setting black background!" << std::endl;
            selectedBackground = -1;
        }
        else if (selectedBackground < 0 || (unsigned) selectedBackground >= backgroundPixmaps.size()) {
            std::cout << "Warning: Selected background out of range, setting black background!" << std::endl;
            selectedBackground = -1;
        }
        else {
            std::cout << "Selected background: " << selectedBackground << std::endl;
        }
    }
    else {
        refresh = false;
    }
    if (refresh) {
        update();
    }
}
